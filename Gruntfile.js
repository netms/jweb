module.exports = function (grunt) {
	const _cache = ".cache",
		  _dist  = "dist";

	// 项目配置
	grunt.initConfig({
		pkg:     grunt.file.readJSON("package.json"),
		// uglify:   {//js压缩
		// 	options: {
		// 		ASCIIOnly: true
		// 	},
		// 	files:   {
		// 		expand: true,
		// 		cwd:    '__src/js', //批匹配相对lib目录的src来源
		// 		src:    '**/*.js', //实际的匹配模式
		// 		dest:   'js' //目标路径前缀
		// 	}
		// },
		// imagemin: {//图片压缩
		// 	options: {
		// 		optimizationLevel: 7
		// 	},
		// 	files:   {
		// 		expand: true,
		// 		cwd:    '__src/images',
		// 		src:    '**/*.{png,jpg,gif}',
		// 		dest:   'images'
		// 	}
		// },
		compass: {
			dist: {
				options: {
					sassDir:     "scss",
					cssDir:      _cache + "/css",
					environment: "production"
				}
			}
		},
		sass:    {
			dist: {
				files: [{
					expand: true,
					cwd:    "scss",
					src:    ["*.scss"],
					dest:   _cache + "/css",
					ext:    ".css"
				}]
			}
		},
		concat:  {
			css: {
				src:  ["css/normalize.css", _cache + "/css/*.css"],
				dest: _cache + "/css/all.css"
			}
		},
		cssmin:  {
			dist: {
				files: [{
					expand: true,
					cwd:    _cache + "/css",
					src:    ["all.css"],
					dest:   _dist + "/css",
					ext:    '.min.css'
				}]
			}
		},
		clean:   {
			cssinit: [".sass-cache", "css/*.css", "!css/normalize.css", _cache, _dist],
			cssdist: [".sass-cache", "css/*.css", "!css/normalize.css", _cache]
		},
		watch:   {
			// uglify:   {
			// 	files: ['__src/js/**/*.js'],
			// 	tasks: ['uglify']
			// },
			// imagemin: {
			// 	files: ['__src/images/**/*.{png,jpg,gif}'],
			// 	tasks: ['imagemin']
			// },
			compass: {
				files: ['__src/css/*.scss'],
				tasks: ['compass']
			}
		}
	});

	// 加载提供"uglify"任务的插件
	// grunt.loadNpmTasks('grunt-contrib-uglify');
	// grunt.loadNpmTasks('grunt-contrib-imagemin');
	// grunt.loadNpmTasks("grunt-contrib-compass");
	grunt.loadNpmTasks("grunt-contrib-sass");
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-cssmin");
	grunt.loadNpmTasks("grunt-contrib-clean");
	// grunt.loadNpmTasks('grunt-contrib-watch');

	// 默认任务
	// grunt.registerTask('default', ['uglify', 'imagemin', 'compass']);
	// grunt.registerTask('default', ['compass']);
	// grunt.registerTask("default", ["compass", "concat", "cssmin"]);
	grunt.registerTask("css", ["clean:cssinit", "sass", "concat:css", "cssmin", "clean:cssdist"]);
};